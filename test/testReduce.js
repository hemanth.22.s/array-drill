const reduce = require("../reduce")

const sumOfArray =function (previousValue, currentValue){
    return previousValue + currentValue;
}

const items = [1, 2, 3, 4, 5, 5]

const result = reduce(items, sumOfArray, 0);

console.log(result)