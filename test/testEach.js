const each = require("../each")

function eachElement(num){
    return num;
}

const items = [1, 2, 3, 4, 5, 5];

const result = each(items,eachElement);

if(result.length > 0){
    for (let index = 0; index<result.length; index++){
        console.log(result[index])
    }
}
else{
    console.log(result)
}

