function filter (elements, cb){
    if(elements == undefined || cb == undefined){
        return []
    } else {

        let primeNumber = []

        for (let index=0; index<elements.length; index ++){
            const isTrue = cd(elements[index])

            if(isTrue){
                primeNumber.push(cb(elements[index]))
            }
        }
        return primeNumber;
    }

}

module.exports = filter;