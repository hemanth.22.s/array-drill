function find(elements, cb) {
    if (elements == undefined || cb == undefined){
        return []
    } else {
        let output = []

        for (index = 0 ; index < elements.length; index ++){
            const isTrue = cb(elements[index])

            if(isTrue){
                output.push(cb(elements[index]))
            }
        }

        return output;
    }
}

module.exports = find;

