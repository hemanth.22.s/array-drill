function each(elements, cb){

    if(elements == undefined || cb == undefined ){
        return []
    }
    else{
        let output = []

        for(let index=0; index < elements.length; index++){

            output.push(cb(elements[index]));
        }
        return output;
    }

}

module.exports = each;
