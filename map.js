function map(elements, cb){
    if (elements == undefined || cb == undefined){
        return []

    } else {

        let newArray = []

        for (let index = 0; index < elements.length; index++){
            
            newArray.push(cb(elements[index]));

        }

        return newArray;
    }

}

module.exports = map;